using System;
using System.Runtime.ConstrainedExecution;
using ColossalFramework;
using ColossalFramework.Math;
using ColossalFramework.Plugins;
using ICities;
using UnityEngine;


// ToDo
// find Roads called "HighwayCount"
// find "Dynamic Message Sign"
// pair them
// get vehicles on highway
// tally passign vehicles
// put tallys on signs

/*
There was some relevant discussion on reddit that could help someone if they attempt to improve on this code:
https://www.reddit.com/r/CitiesSkylines/comments/133mye7/i_created_a_mod_for_counting_vehiclestraffic/

In particular, the comment from krzychu124, an experienced modder who took the time to look at the code:
    "Nice idea, but you could count vehicles on simulation thread not the main (render), so CPU performance won't make any difference (as you've noticed).
    Also m_pathPositionIndex is 2x of path positions count, because even number point to actual pathUnit position and odd number is when vehicle is in transition between pathUnits, so to get correct index you need to pass pathPositionIndex >> 1 to pathUnit.GetPosition() (divide by 2).
    Additionally you should try utilize pathUnit.m_nextPathUnit to figure out next position instead of trying to find previous. PathUnits can be traversed forward only so it's not really possible to efficiently find previous PathUnit - vehicle is not where you think it is :)"

*/



namespace MyFirstMod
{
    public class MyFirstMod : IUserMod
    {

        

        public string Name
        {
            get { return "My First Mod"; }
        }

        public string Description
        {
            get { return "My First Mod by RichAntDav"; }
        }

    }

    public class Loader : LoadingExtensionBase
    {
        public override void OnLevelLoaded(LoadMode mode)
        {
            GameObject go = new GameObject("Test Object");
            go.AddComponent<MyBehaviour>();
        }
    }

    public class MyBehaviour : MonoBehaviour
    {
        private VehicleManager vMgr = Singleton<VehicleManager>.instance;
        private NetManager nMgr = Singleton<NetManager>.instance;
        private PathManager pMgr = Singleton<PathManager>.instance;

        private bool didInit = false;

        private System.Collections.Generic.List<int> hcSegments = new System.Collections.Generic.List<int>();
        private System.Collections.Generic.List<bool> hcIsIn = new System.Collections.Generic.List<bool>();
        private System.Collections.Generic.List<int> numVehiclesLeavingSegmentTally = new System.Collections.Generic.List<int>();
        private System.Collections.Generic.List<System.Collections.Generic.List<int>> vehiclesInCountZoneCurrent = new System.Collections.Generic.List<System.Collections.Generic.List<int>>();
        private System.Collections.Generic.List<System.Collections.Generic.List<int>> vehiclesInCountZoneNew = new System.Collections.Generic.List<System.Collections.Generic.List<int>>();

        int frameCounter = 0;

        int prevTime = -1;
        const int DIVISOR = 60; // number of seconds to tally vehicle movements, going to debug-out the totals every this number of seconds
        const int COUNT_FRAME_INTERVAL = 10; // Going to check the cars leaving the watched segments every "COUNT_FRAME_INTERVAL" number of frames

        void Start()
        {
            // later

            //DebugOutputPanel.AddMessage(PluginManager.MessageType.Message, "first mod works, nice");
            Debug.Log("first mod works, nice");

            prevTime = (int)Math.Floor(Time.time / DIVISOR);

        }

        void Update()
        {

            frameCounter++;
            if (frameCounter > COUNT_FRAME_INTERVAL)
            {
                if(didInit == false)
                {
                    findRoads();
                    findCurrentVinCZ();
                    debugResults();

                    vehiclesInCountZoneCurrent = vehiclesInCountZoneNew;

                    didInit = true;
                } else
                {
                    findCurrentVinCZ();
                    debugResults();

                    // check the difference between current and new
                    countVehliclesLeavingSegment();

                    vehiclesInCountZoneCurrent = vehiclesInCountZoneNew;
                }


                frameCounter = 0;



                // plan 
                // flexible time period, say start with 10s

                // count the cars which are in the new array but not in the current array


            }

            if(didInit == true)
            {

                float currentTime = Time.time;
                int roundedDown = (int)Math.Floor(currentTime / DIVISOR);

                //Debug.Log("Tick: prevTime = " + prevTime + ", roundedDown = " + roundedDown);

                if (roundedDown > prevTime)
                {
                    prevTime = roundedDown;
                    string debugStr = "TICK:";

                    int inTotal = 0;
                    int outTotal = 0;

                    for (int i = 0; i < hcSegments.Count; i++)
                    {
                        string inout = "<OUT>";
                        if(hcIsIn[i] == true)
                        {
                            inout = "<IN>";
                            inTotal += numVehiclesLeavingSegmentTally[i];
                        } else
                        {
                            outTotal += numVehiclesLeavingSegmentTally[i];
                        }

                        // can comment out the following line if you don't want to see the individual segment totals
                        debugStr += "\nSeg " + inout + " {" + i + "," + hcSegments[i] + "} = " + numVehiclesLeavingSegmentTally[i];
                    }

                    debugStr += "\nIN TOTAL = " + inTotal + ", OUT TOTAL = " + outTotal;

                    Debug.Log(debugStr);

                    // reset the tallies fro the next time period
                    for (int i = 0; i < hcSegments.Count; i++)
                    {
                        numVehiclesLeavingSegmentTally[i] = 0;
                    }

                }

            }

        }

        void countVehliclesLeavingSegment()
        {
            // count vehicles per segment, as well as total number per all watched segments

            string fullDebug = "Vehicles passing through segment:";

            // look at each segment
            for(int i = 0; i < hcSegments.Count; i++)
            {
                string debugSeg = "Seg[" + i + "] (" + hcSegments[i] + ") = ";

                int numVinNewSeg = 0;
                int vehiclesInCountZoneCurrentInCurrentButNotNew = 0;

                for (int j=0; j< vehiclesInCountZoneCurrent[i].Count; j++)
                {
                    numVinNewSeg++;
                    int vId = vehiclesInCountZoneCurrent[i][j];
                    bool found = false;
                    for (int k = 0; k < vehiclesInCountZoneNew[i].Count; k++)
                    {
                        if(vId == vehiclesInCountZoneNew[i][k])
                        {
                            found = true;
                        }
                    }

                    if(found == false)
                    {
                        vehiclesInCountZoneCurrentInCurrentButNotNew++;
                    }

                }

                debugSeg += vehiclesInCountZoneCurrentInCurrentButNotNew;
                fullDebug = fullDebug + "\n" + debugSeg;

                numVehiclesLeavingSegmentTally[i] += vehiclesInCountZoneCurrentInCurrentButNotNew;

            }

            //Debug.Log(fullDebug);

            vehiclesInCountZoneCurrent = vehiclesInCountZoneNew;
        }



        void debugResults()
        {
            string debugStr = "";

            for (int i=0; i< hcSegments.Count; i++)
            {
                string segStr = "";
                segStr += "Segment [" + i + "] id: " + hcSegments[i] + ", vehicles:";
                for(int j=0;  j< vehiclesInCountZoneNew[i].Count; j++)
                {
                    segStr += vehiclesInCountZoneNew[i][j] + ",";
                }

                debugStr += segStr + "\n";
            }

            //DebugOutputPanel.AddMessage(PluginManager.MessageType.Message, debugStr);
            //Debug.Log(debugStr);
        }

        void finalResults()
        {

        }

        void findRoads()
        {
            int numRoads = -1;

            Debug.Log("start of findRoads");

            for (ushort i =0; i< nMgr.m_segments.m_buffer.Length; i++)
            {
                NetSegment seg = nMgr.m_segments.m_buffer[i];
                
                if (seg.Info != null) {

                    if (((seg.Info.name == "HighwayCountIn.HighwayCountIn_Data") || (seg.Info.name == "Highway4CountIn.Highway4CountIn_Data")) && (seg.m_averageLength > 1.0))
                    {
                        Debug.Log("name: " + i + ", = " + seg.Info.name + ", len = " + seg.m_averageLength + ", flags =" + seg.m_flags);

                        hcSegments.Add(i);
                        hcIsIn.Add(true);
                        numVehiclesLeavingSegmentTally.Add(0);
                        vehiclesInCountZoneCurrent.Add(new System.Collections.Generic.List<int>());
                        vehiclesInCountZoneNew.Add(new System.Collections.Generic.List<int>());

                    } else if (((seg.Info.name == "HighwayCountOut.HighwayCountOut_Data") || (seg.Info.name == "Highway4CountOut.Highway4CountOut_Data")) && (seg.m_averageLength > 1.0))
                    {
                        Debug.Log("name: " + i + ", = " + seg.Info.name + ", len = " + seg.m_averageLength + ", flags =" + seg.m_flags);

                        hcSegments.Add(i);
                        hcIsIn.Add(false);
                        numVehiclesLeavingSegmentTally.Add(0);
                        vehiclesInCountZoneCurrent.Add(new System.Collections.Generic.List<int>());
                        vehiclesInCountZoneNew.Add(new System.Collections.Generic.List<int>());

                    }
                }

            }
            


            /*for (ushort i = 1; i < nMgr.m_nodes.m_buffer.Length; i++)
            {
                NetNode n = nMgr.m_nodes.m_buffer[i];

                NetSegment seg = nMgr.m_segments.m_buffer[i]

                n.GetSegment(234);
                n.GetSegment(0);

                if (n.Info == null) i = 0; 
                {
                    Debug.Log("Null, loop: " + i);
                } else
                {
                    if (n.Info.name == "HighwayCountIn.HighwayCountIn_Data")
                    {
                        ushort segIdZero = n.GetSegment(0);
                        String segmentZeroName = nMgr.GetSegmentName(segIdZero);

                        if (segmentZeroName == "HighwayCountIn.HighwayCountIn_Data")
                        {
                            hcSegments.Add(segIdZero);
                            hcIsIn.Add(true);
                            numVehiclesLeavingSegmentTally.Add(0);
                            vehiclesInCountZoneCurrent.Add(new System.Collections.Generic.List<int>());
                            vehiclesInCountZoneNew.Add(new System.Collections.Generic.List<int>());
                        }

                        ushort segIdOne = n.GetSegment(1);
                        String segmentOneName = nMgr.GetSegmentName(segIdOne);

                        if (segmentOneName == "HighwayCountIn.HighwayCountIn_Data")
                        {
                            hcSegments.Add(segIdOne);
                            hcIsIn.Add(true);
                            numVehiclesLeavingSegmentTally.Add(0);
                            vehiclesInCountZoneCurrent.Add(new System.Collections.Generic.List<int>());
                            vehiclesInCountZoneNew.Add(new System.Collections.Generic.List<int>());
                        }

                        Debug.Log("IN> segmentZeroName = " + segmentZeroName + ", segmentOneName = " + segmentOneName);
                    }
                    else if (n.Info.name == "HighwayCountOut.HighwayCountOut_Data")
                    {
                        ushort segIdZero = n.GetSegment(0);
                        String segmentZeroName = nMgr.GetSegmentName(segIdZero);
                        nMgr.GetSegment();

                        if (segmentZeroName == "HighwayCountOut.HighwayCountOut_Data")
                        {
                            hcSegments.Add(segIdZero);
                            hcIsIn.Add(true);
                            numVehiclesLeavingSegmentTally.Add(0);
                            vehiclesInCountZoneCurrent.Add(new System.Collections.Generic.List<int>());
                            vehiclesInCountZoneNew.Add(new System.Collections.Generic.List<int>());
                        }

                        ushort segIdOne = n.GetSegment(1);
                        String segmentOneName = nMgr.GetSegmentName(segIdOne);

                        if (segmentOneName == "HighwayCountOut.HighwayCountOut_Data")
                        {
                            hcSegments.Add(segIdOne);
                            hcIsIn.Add(true);
                            numVehiclesLeavingSegmentTally.Add(0);
                            vehiclesInCountZoneCurrent.Add(new System.Collections.Generic.List<int>());
                            vehiclesInCountZoneNew.Add(new System.Collections.Generic.List<int>());
                        }

                        Debug.Log("OUT> segmentZeroName = " + segmentZeroName + ", segmentOneName = " + segmentOneName);
                    }

                    else
                    {
                        numRoads++;
                    }
                }

            }*/

            string debugStr = "findRoads: Total number of unwatched roads = " + numRoads + ", number of watched segments = " + hcSegments.Count;

            //DebugOutputPanel.AddMessage(PluginManager.MessageType.Message, debugStr);
            Debug.Log(debugStr);

            //didRoads = true;
        }

        void findCurrentVinCZ()
        {
            int numBikes = 0;
            int numVehicles = 0;

            // create a new List of Lists for the vehicles in the count zones
            vehiclesInCountZoneNew = new System.Collections.Generic.List<System.Collections.Generic.List<int>>();
            for (int segItem = 0; segItem < hcSegments.Count; segItem++)
            {
                vehiclesInCountZoneNew.Add(new System.Collections.Generic.List<int>());
            }


            for (ushort i = 1; i < vMgr.m_vehicles.m_buffer.Length; i++)
            {
                Vehicle v = vMgr.m_vehicles.m_buffer[i];
                //first we need to only trigger on the vehicles we're interested in, they must be created and have info objects.
                if ((v.m_flags & Vehicle.Flags.Created) == Vehicle.Flags.Created && v.Info != null)
                {
                    //Ok so we're flagged as created
                    if (v.Info.m_vehicleType == VehicleInfo.VehicleType.Bicycle | v.Info.m_vehicleType == VehicleInfo.VehicleType.None)
                    {
                        numBikes++;
                    }
                    else
                    {
                        numVehicles++;

                        uint pathId = v.m_path;
                        byte pathPosInd = v.m_pathPositionIndex;

                        PathUnit pu = pMgr.m_pathUnits.m_buffer[pathId];
                        
                        PathUnit.Position pos = pu.GetPosition(pathPosInd);
                        ushort segmentId = pos.m_segment;

                        ushort segmentIdPrev = 0;

                        if (pathPosInd > 0)
                        {
                            PathUnit.Position posB = pu.GetPosition(pathPosInd - 1);
                            segmentIdPrev = posB.m_segment;
                        }
                        
                        // check if vehicle is in any of the segments we are watching
                        for (int segItem=0; segItem < hcSegments.Count; segItem++)
                        {
                            if((hcSegments[segItem] == segmentId) || (hcSegments[segItem] == segmentIdPrev))
                            {
                                vehiclesInCountZoneNew[segItem].Add(i);

                                //Debug.Log("findCurrentVinCZ: i=" + i + ", pathId = " + pathId + ", pathPosInd = " + pathPosInd + ", segmentId = " + segmentId + ", segmentIdPrev = " + segmentIdPrev);

                            }

                        }
                    }
                }
            }

        }


        
    }
}



