# Traffic Counting mod for Cities:Skylines by RichAntDav

The code in the file MyFirstMod.cs contains all the functionality for a very basic traffic counting mod that I wrote a little while ago.

## Warning 1
The code is not very good, sorry. This was the first and so far only bit of code I've ever written in microsofts c-sharp. As such I'd be embarassed to put this on my personal github account in case any future prospective employer ever saw it!

## Warning 2
This mod is not user-firendly. It relies on the user adding and then using custom road segments with a very specific name ("HighwayCountIn" and "HighwayCountOut"). Also, the only output is to the debug console.

## Warning 3
I wrote this with testing highway intersections in mind, hence the HighwayCountOut and HighwayCountIn names, but in the debug console you can see the number of vehicle transitions for every single node, so it can be used for other purposes if so desired.

## Warning 4
something messes up if you re-load your city or load up a new one, the workaound is to re-start cities:skylines!

## Resources
The instructions for installing and using the mod can be found here: https://steamcommunity.com/sharedfiles/filedetails/?id=2338755918

Also, quick shout-out to the tutorial that I used to write the mod in the first place: https://community.simtropolis.com/forums/topic/73404-modding-tutorial-0-your-first-mod/
Note: If you follow those tutorial instructions for installing the mod code you should be good to go. After that you need to create a new custom type of road (or two) called "HighwayCountIn" and "HighwayCountOut" (it has to be exactly those names) then you should see the results in the debug console.












